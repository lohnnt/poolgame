#include "stage3cueballdecorator.h"

#include <QDebug>

Stage3CueBallDecorator::Stage3CueBallDecorator(Ball *b, Dialog *parent)
    :BallDecorator(b),clicked(false)
{
    //connect dialog signals to cueball slots so that this class is notified when the mouse is used
    connect(parent,&Dialog::mousePressed,this,&Stage3CueBallDecorator::mousePressed);
    connect(parent,&Dialog::mouseMoved,this,&Stage3CueBallDecorator::mouseMoved);
    connect(parent,&Dialog::mouseReleased,this,&Stage3CueBallDecorator::mouseReleased);
}

void Stage3CueBallDecorator::draw(QPainter &p)
{
    m_ball->draw(p);
    if(clicked){
        p.drawLine(mousePos.toPointF(),m_ball->position().toPointF());

        double newX =  mousePos.x() - m_ball->position().x();
        double newY = mousePos.y() - m_ball->position().y();

        QVector2D a (m_ball->position().x() - newX, m_ball->position().y() - newY);
        p.drawLine(a.toPointF(), m_ball->position().toPointF() );
    }

}

void Stage3CueBallDecorator::mousePressed(QMouseEvent *event)
{
    if(velocity().lengthSquared()<0.001 && (QVector2D(event->pos())-position()).length() < radius())
    {
        clicked = true;
        mousePos = QVector2D(event->pos());
    }
}

void Stage3CueBallDecorator::mouseMoved(QMouseEvent *event)
{
    if(clicked)
    {
        mousePos = QVector2D(event->pos());
    }

}

void Stage3CueBallDecorator::mouseReleased(QMouseEvent *event)
{
    if(clicked)
    {
        clicked = false;
        setVelocity(4*(m_ball->position()-mousePos));
    }
}
