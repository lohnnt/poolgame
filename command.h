#ifndef COMMAND_H
#define COMMAND_H

#include "commandreceiver.h"

class Command{
public:
    virtual void execute() = 0;

};


//// List of commands down below


/**
 * @brief The SaveCommand class - will have the state at the provided position from the memento list
 */
class SaveCommand: public Command
{
    CommandReceiver *m_save;
public:
    SaveCommand(CommandReceiver *save)
    : m_save(save){}

    void execute() {m_save->saveState();}
};
/**
 * @brief The LoadCommand class - will get the state at the provided position from the memento list
 */
class LoadPrevCommand : public Command
{
    CommandReceiver *m_loadPrev;
public:
    LoadPrevCommand(CommandReceiver *load)
    : m_loadPrev(load){}

    void execute() {m_loadPrev->loadPrevState();}

};

/**
 * @brief The LoadCommand class - will get the state at the provided position from the memento list
 */
class LoadNextCommand : public Command
{
    CommandReceiver *m_loadNext;
public:
    LoadNextCommand(CommandReceiver *load)
    : m_loadNext(load){}

    void execute() {m_loadNext->loadNextState();}

};


/**
 * @brief The ChangeColourCommand class - will change the colour of all balls excluding the cueball
 */
class ChangeColourCommand : public Command
{
    CommandReceiver *m_ballCol;
public:
    ChangeColourCommand(CommandReceiver *ballCol): m_ballCol(ballCol){}

    void execute(){m_ballCol->changeColour();}

};

#endif // COMMAND_H
