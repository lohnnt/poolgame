#ifndef COMMANDRECEIVER_H
#define COMMANDRECEIVER_H


#include"caretaker.h"
#include"memento.h"
#include"originator.h"

class CommandReceiver
{
    PoolGame * m_game;
    Originator * m_originator;
    CareTaker * m_careTaker;
    int position = 0;
    int maxPosition = 0;

public:

    CommandReceiver(PoolGame * game, Originator* originator, CareTaker* careTaker)
        :m_game(game)
        ,m_originator(originator)
        ,m_careTaker(careTaker){}

    /**
     * @brief saveState - will save the state of the memento
     * @param position - provided position to save
     */
    void saveState();

    /**
     * @brief loadState - will load the previous state of the memento
     * @param position - the position to load
     */
    void loadPrevState();

    /**
     * @brief loadState - will load the next state of the memento
     * @param position - the position to load
     */
    void loadNextState();

    /**
     * @brief changeColour - will change the colour of all ball (ncluding children) except the cueBall
     */
    void changeColour();

};

#endif // COMMANDRECEIVER_H
