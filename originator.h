#ifndef ORIGINATOR_H
#define ORIGINATOR_H

#include"memento.h"
#include<vector>
#include<string>
#include<QString>

/**
 * @brief The Originator class - Handles the states
 */
class Originator
{
public:
    Originator() {}

    /**
     * @brief setState - updates the state private variable
     * @param state - the new state to place
     */

    void setState(std::vector<Ball*> state)
    {
        this->state = state;
    }

    /**
     * @brief getState - gets the current state
     * @return - the current state
     */
    std::vector<Ball*> getState()
    {
        return state;
    }

    /**
     * @brief saveStateToMemento - stores the state to Memento
     * @return - a saved Memento state
     */
    Memento saveStateToMemento()
    {
        return Memento(state);
    }

    /**
     * @brief getStateFromMemento - retrieves the state from Memento
     * @param memento - The memenolist to call from
     */
    void getStateFromMemento(Memento memento)
    {
        state = memento.getState();
    }

private:
    std::vector<Ball*> state;
};

#endif // ORIGINATOR_H
