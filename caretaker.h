#ifndef CARETAKER_H
#define CARETAKER_H


#include"memento.h"
#include"poolgame.h"
#include<vector>
#include<string>
#include<QString>
#include<QDebug>

class Dialog;

/**
 * @brief The CareTaker class - store memento
 */
class CareTaker{
public:
    CareTaker() {}
    CareTaker(Dialog *parent):
        m_parent(parent){}


    /**
     * @brief add - inserts a new memento state at the given position
     * @param state - the memento state to insert
     * @param position - the position to insert it into
     */
    void add(Memento state, int position);

    /**
     * @brief createSimpleBall - deep copy a new SimpleBall
     * @param a - the targeted ball to copy
     * @return - a new Simple Ball
     */
    SimpleStage2Ball* createSimpleBall(Ball* a);

    /**
     * @brief createCompositeBall - deep copy a new CompositeBall
     * @param a - the target ball to copy
     * @return - a new composite ball
     */
    CompositeBall* createCompositeBall(Ball* a);


    /**
     * @brief get - retrieves the memento from the mementoList
     * @param index - the target position to get
     * @return - the targeted position from the memento list
     */
    Memento get(int index){
        return mementoList.at(index);
    }


private:
    std::vector<Memento> mementoList;
    Dialog * m_parent;
};

#endif // CARETAKER_H
