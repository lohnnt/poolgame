#ifndef POCKET_H
#define POCKET_H

#include <QPainter>
#include <QVector2D>

/**
 * @brief The Pocket class simply represents a pocket on the table
 */
class Pocket
{
public:
    Pocket(QVector2D pos, float r)
        :m_position(pos),m_radius(r)
    {}
    void draw(QPainter &p);
    /**
     * @brief encompassed - checks if the child ball is contained in the parent ball
     * @param circleCentre - The point of the centre of the parent ball
     * @param r - the radius of the child ball
     * @return true if the child ball is contained inside the parent, otherwise false
     */
    bool encompassed(QVector2D circleCentre, float r)
    {
        return (circleCentre-m_position).length() + r < m_radius;
    }

private:
    QVector2D m_position;
    float m_radius;
};

#endif // POCKET_H
