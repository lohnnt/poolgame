#include "dialog.h"

#include <QPainter>
#include <QSize>
#include <QDebug>

constexpr float fps = 60;
constexpr float timeStep = 0.01;

Dialog::Dialog(QWidget *parent)
        :QDialog(parent),
          m_game(nullptr),
          m_framerateTimer(new QTimer()),
          m_timestepTimer(new QTimer())
{

}

void Dialog::start(PoolGame *game)
{
    m_game = game;
    this->setMinimumSize(m_game->size());
    this->resize(m_game->size());
    connect(m_framerateTimer,SIGNAL(timeout()),this,SLOT(update()));
    connect(m_timestepTimer,SIGNAL(timeout()),this,SLOT(runSimulationStep()));
    m_framerateTimer->start(1000/fps);
    m_timestepTimer->start(1000*timeStep);

    if(m_stage3){
        // memento set up
        originator = new Originator();
        careTaker = new CareTaker(this);
        // Command set up
        control = new CommandInvoker;
        commandSet = new CommandReceiver(m_game,originator,careTaker);
        // list of commands set up
        saveState = new SaveCommand(commandSet);
        loadPrevState = new LoadPrevCommand(commandSet);
        loadNextState = new LoadNextCommand(commandSet);
        changeColour = new ChangeColourCommand(commandSet);

        control->setCommand(saveState);
        control->executeCommand();
    }

}

void Dialog::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    m_game->draw(p);
}

void Dialog::mousePressEvent(QMouseEvent *event)
{
    emit mousePressed(event);
}

void Dialog::mouseMoveEvent(QMouseEvent *event)
{
    emit mouseMoved(event);
}

void Dialog::mouseReleaseEvent(QMouseEvent *event)
{
    if(m_stage3){
       if(event->button() == Qt::LeftButton){
           control->setCommand(saveState);
           control->executeCommand();
        }
    }
    emit mouseReleased(event);
}


void Dialog::keyPressEvent(QKeyEvent *event)
{

    if(m_stage3)
    {
        if (event->text() == "r"){
            // to go back to a previous revision
            control->setCommand(loadPrevState);
            control->executeCommand();
        }
        else if(event->text() == "f"){
            // go to a forward revision
            control->setCommand(loadNextState);
            control->executeCommand();
        }
        else if(event->text() == "c"){
            // change colour of the balls except the cueball
            control->setCommand(changeColour);
            control->executeCommand();
        }
    }
}

void Dialog::stage3Check(bool stage3){
    if(stage3){
        m_stage3 = true;
    }
    else{
        m_stage3 = false;
    }
}

Dialog::~Dialog()
{
    delete m_game;
    delete m_framerateTimer;
    delete m_timestepTimer;
    delete originator;
    delete careTaker;
    delete commandSet;
    delete control;
}

void Dialog::runSimulationStep()
{
    m_game->simulateTimeStep(timeStep);
}
