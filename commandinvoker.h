#ifndef COMMANDINVOKER_H
#define COMMANDINVOKER_H

#include"command.h"
#include"commandreceiver.h"

class CommandInvoker
{
    Command *m_command;
public:
    /**
     * @brief setCommand - load a command up
     * @param cmd - the command being loaded
     */
    void setCommand(Command *cmd){
        m_command = cmd;
    }

    /**
     * @brief executeCommand - run the command
     */
    void executeCommand() {
        m_command->execute();
    }
};

#endif // COMMANDINVOKER_H
