#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QWidget>
#include <QPaintEvent>
#include <QTimer>
#include <QMouseEvent>

#include "poolgame.h"
#include "originator.h"
#include "caretaker.h"
#include "command.h"
#include "commandinvoker.h"
#include "commandreceiver.h"


#include <QKeyEvent>

/**
 * @brief The Dialog class starts up and displays a poolgame
 */
class Dialog : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Dialog constructor
     * @param game is a pointer to a PoolGame, this takes ownership of that pointer
     * @param parent is the parent widget
     */
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    /**
     * @brief starts the simulation
     */
    void start(PoolGame * game);

    /**
     * @brief draws the simulation
     */
    void paintEvent(QPaintEvent *);

    /**
     * @brief mousePressEvent just emits the mousePressed signal and/or stage3 components
     * @param event
     */
    void mousePressEvent(QMouseEvent *event);

    /**
     * @brief mousePressEvent just emits the mouseMoved signal and/or stage3 components
     * @param event
     */
    void mouseMoveEvent(QMouseEvent *event);

    /**
     * @brief mousePressEvent just emits a mouseReleased signal and/or stage3 components
     * @param event
     */
    void mouseReleaseEvent(QMouseEvent *event);

    /**
     * @brief keyPressEvent - checks the keys for the stage  3 component
     * @param event
     */
    void keyPressEvent(QKeyEvent * event);
    /**
     * @brief stage3Check - flag for stage3
     * @param stage3 - the variable to set the stage3 flah.
     */
    void stage3Check(bool stage3);




signals:
    void mousePressed(QMouseEvent * event);
    void mouseMoved(QMouseEvent * event);
    void mouseReleased(QMouseEvent * event);

public slots:
    void runSimulationStep();

private:
    PoolGame * m_game;
    QTimer * m_framerateTimer;
    QTimer * m_timestepTimer;
    Originator * originator;
    CareTaker * careTaker;
    bool m_stage3 = false;
    SaveCommand *saveState;
    LoadPrevCommand *loadPrevState;
    LoadNextCommand *loadNextState;
    ChangeColourCommand *changeColour;


    CommandReceiver *commandSet;
    CommandInvoker *control;


};

#endif // DIALOG_H
