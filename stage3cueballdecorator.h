#ifndef STAGE3CUEBALLDECORATOR_H
#define STAGE3CUEBALLDECORATOR_H

#include "balldecorator.h"

#include "dialog.h"

#include <QWidget>
#include <QPainter>
#include <QMouseEvent>

/**
 * @brief The CueBallDecorator class is the decorator that turns a ball into a cue ball
 */
class Stage3CueBallDecorator : public BallDecorator, public QObject
{
public:
    /**
     * @brief CueBallDecorator decorates a ball to be used as a cue ball
     * @param b is the ball to be decorated
     * @param parent is the parent Dialog, this is used to connect to the signals it emits
     */
    Stage3CueBallDecorator(Ball * b, Dialog * parent);

    void draw(QPainter &p);
public slots:
    void mousePressed(QMouseEvent * event);
    void mouseMoved(QMouseEvent * event);
    void mouseReleased(QMouseEvent * event);

private:
    QVector2D endpoint;
    QVector2D mousePos;
    bool clicked;

};

#endif // STAGE3CUEBALLDECORATOR_H
