#ifndef CHANGEINPOOLGAME_H
#define CHANGEINPOOLGAME_H
#include <vector>
class Ball;

/**
 * @brief The ChangeInPoolGame class stores a change that will be made to the balls vector, it is used by functions that
 * wish to effect the balls vector but don't have direct access to poolgame
 */
class ChangeInPoolGame
{
public:
    ChangeInPoolGame(std::vector<Ball*> ballsToRemove = std::vector<Ball*>(),std::vector<Ball*> ballsToAdd = std::vector<Ball*>())
        :m_ballsToRemove(ballsToRemove),m_ballsToAdd(ballsToAdd)
    {}
    std::vector<Ball *> m_ballsToRemove;
    std::vector<Ball *> m_ballsToAdd;
    /**
     * @brief empty - a function to empty the std::vector<Ball *> arrays
     * @return true if both m_ballsToRemove and m_ballsToAdd are empty
     */
    bool empty() const{
        return m_ballsToRemove.empty()&&m_ballsToAdd.empty();
    }
    /**
     * @brief ChangeInPoolGame::merge merges two changes together and returns the total change
     * @param other
     * @return
     */
    ChangeInPoolGame merge(const ChangeInPoolGame &other);
};

#endif // CHANGEINPOOLGAME_H
