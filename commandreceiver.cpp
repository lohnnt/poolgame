#include "commandreceiver.h"

void CommandReceiver::saveState()
{
    m_originator->setState(m_game->getBalls());
    m_careTaker->add(m_originator->saveStateToMemento(), position);
    position +=1;
    maxPosition +=1;
}

void CommandReceiver::loadPrevState()
{
    position -=1;
    if(position < 0 ){
        position =0;
    }
    m_originator->getStateFromMemento(m_careTaker->get(position));
    m_game->setBalls(m_originator->getState());

}

void CommandReceiver::loadNextState()
{
    position +=1;
    if(position > maxPosition){
        position = maxPosition;
    }
    m_originator->getStateFromMemento(m_careTaker->get(position));
    m_game->setBalls(m_originator->getState());

}

void CommandReceiver::changeColour()
{
    std::vector<Ball*> listBall = m_game->getBalls();
    for(size_t i = 0; i < listBall.size(); ++i){
        Ball* a = listBall[i];
        if(dynamic_cast<CompositeBall*>(a) != nullptr){
            // change the children's colour
            CompositeBall* b = dynamic_cast<CompositeBall*>(a);
            for(size_t i = 0; i < b->getContainedBalls().size(); ++i){
                Ball* c= b->getContainedBalls()[i];
                c->setColour(QColor(rand()%255,rand()%255, rand()%255));
            }
        }
        // change all balls except the cueball
        if (a->colour() != "white"){
            a->setColour(QColor(rand()%255,rand()%255, rand()%255));
        }
    }
    m_game->setBalls(listBall);
}



