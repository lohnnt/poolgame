#include "caretaker.h"
#include "ball.h"
#include "poolgame.h"
#include "stage2ball.h"

#include "stage3cueballdecorator.h"
#include "balldecorator.h"


SimpleStage2Ball* CareTaker::createSimpleBall(Ball* a){
    // deep copy Simple ball
    SimpleStage2Ball* c =dynamic_cast<SimpleStage2Ball*>(a);
    SimpleStage2Ball* d = new SimpleStage2Ball();
    d->setMass(c->mass());
    d->setColour(c->colour());
    d->setPosition(c->position());
    d->setVelocity(c->velocity());
    d->setRadius(c->radius());
    d->setStrength(c->getStrength());
    return d;

}

CompositeBall* CareTaker::createCompositeBall(Ball* a){
    // deep copy Composite Ball
    CompositeBall * c = dynamic_cast<CompositeBall*>(a);
    CompositeBall * d = new CompositeBall();
    d->setContainedMass(c->getContainedMass());
    d->setColour(c->colour());
    d->setPosition(c->position());
    d->setVelocity(c->velocity());
    d->setRadius(c->radius());
    d->setStrength(c->getStrength());
    std::vector<Ball*> newChildrenList;

    for (size_t i = 0; i < c->getContainedBalls().size(); ++i){
        Ball* childBall = c->getContainedBalls()[i];
        if(dynamic_cast<CompositeBall*>(childBall) != nullptr){
            CompositeBall* e = createCompositeBall(c->getContainedBalls()[i]);
            Ball* f = e;
            newChildrenList.push_back(f);
        }
        else if(dynamic_cast<SimpleStage2Ball*>(childBall) != nullptr){
            SimpleStage2Ball* e = createSimpleBall(c->getContainedBalls()[i]);
            Ball* f = e;
            newChildrenList.push_back(f);
        }
    }
    d->setContainedBalls(newChildrenList);
    return d;
}


void CareTaker::add(Memento state, int position){

    std::vector<Ball*> oldSet = state.getState();
    std::vector<Ball*> cloneSet;

    for (size_t i = 0; i < oldSet.size() ; ++i){

        Ball* a = oldSet[i];
        Ball* b = new Stage2Ball();

        if(dynamic_cast<Stage3CueBallDecorator*>(a) != nullptr){
            // deep copy Stage3 Cue Ball Decorator
            Ball* c = dynamic_cast<Stage3CueBallDecorator*>(oldSet[i]);
            Ball* d = new Stage3CueBallDecorator(b,m_parent);
            d->setMass(c->mass());
            d->setColour(c->colour());
            d->setPosition(c->position());
            d->setVelocity(c->velocity());
            d->setRadius(c->radius());
            b = d;
        }
        else if (dynamic_cast<SimpleStage2Ball*>(a) != nullptr){
            b= createSimpleBall(a);
        }

        else if (dynamic_cast<CompositeBall*>(a) != nullptr){
            b = createCompositeBall(a);
        }
        cloneSet.push_back(b);
    }
    mementoList.insert(mementoList.begin() + position, cloneSet);
}
