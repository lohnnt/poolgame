#ifndef MEMENTO_H
#define MEMENTO_H

#include<vector>
#include<string>
#include<QString>
#include"ball.h"

/**
 * @brief The Memento class - stores the state
 */
class Memento
{
public:

    Memento(std::vector<Ball*> state)
    {
        this->m_state = state;
    }
    /**
     * @brief getState - retrieves the state from std vector ball array
     * @return  - the current state of the memento
     */
    std::vector<Ball*> getState()
    {
        return m_state;
    }
private:
    std::vector<Ball*> m_state;
    friend class Originator;
};


#endif // MEMENTO_H
